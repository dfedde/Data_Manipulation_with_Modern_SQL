# 1 Data Manipulation with Modern SQL

## 1.1 About Me

* Chris Fedde
* chris@fedde.us
* cfedde most other places where I have an id.
* Not a facebook, twitter, social media user much.

## 1.2 Intro

## 1.3 Intro

* Intros that drone

## 1.4 Intro

* Intros that drone
* On

## 1.5 Intro

* Intros that drone
* On
* .red[And] on

## 1.6 Intro

* Intros that drone.
* On
* And on
* Are boring.

## 1.7 Lets .red[get] started!

# 2 Explore the repo

## 2.1 Explore the repo

Check it out of gitlab

## 2.2 Explore the repo 

Check it out of gitlab

* https://gitlab.com/cfedde/Data_Manipulation_with_Modern_SQL
* git@gitlab.com:cfedde/Data_Manipulation_with_Modern_SQL.git
* https://gitlab.com/cfedde/Data_Manipulation_with_Modern_SQL.git

## 2.3 Explore the repo 

Check it out of gitlab

One way to check it out is as follows.

```
git clone https://gitlab.com/cfedde/Data_Manipulation_with_Modern_SQL.git
cd Data_Manipulation_with_Modern_SQL
```

## 2.4 Explore the repo

Look at the file structure.

### 2.4.1 Explore the repo

```shell
tree . | less
```

### 2.4.2 Explore the repo

```shell
ls -CR --color | less -R
```

## 2.5 Work Through The Exercises.

# 3 Notes:

## 3.1 Notes

* .red[Examples are just examples.]

## 3.2 Notes

* Examples are just examples.
* Everything I produced here is released to the .red[public domain].

## 3.3 Notes

* Examples are just examples.
* Everything I produced here is released to the public domain
* .red[Everything else] I used has its own license and copyright

## 3.4 Notes

* Call me out on my .red[mistakes].

## 3.5 Notes

* Call me out on my mistakes
* Interrupt early. .red[Interrupt] often

## 3.6 Notes

* Call me out on my mistakes
* Interrupt early. Interrupt often
* Use the .red[5] minute rule:

## 3.7 Notes

* Call me out on my mistakes
* Interrupt early. Interrupt often
* Use the 5 minute rule: If you cannot make it work in 5 minutes 
  .red[ask] for help.

## 3.8 Notes

* Patches .red[Welcome]

## 3.9 Notes

* Patches Welcome
* Gitlab .red[issues]

## 3.10 Notes

* Patches Welcome
* Gitlab issues
* Merge requests

## 3.11 Notes

* Patches Welcome
* Gitlab issues
* Merge requests
* Notes tied to .red[rocks] thrown through windows

## 3.12 Workflow

* This is "See One, Do One, Teach One"

## 3.13 Workflow

* This is "See One, Do One, Teach One"
* See one:

## 3.14 Workflow

* This is "See One, Do One, Teach One"
* See one: I walk through .red[the] section

## 3.15 Workflow

* This is "See One, Do One, Teach One"
* See one: I walk through the section
* Do one: You do what I .red[just] did

## 3.16 Workflow

* This is "See One, Do One, Teach One"
* See one: I walk through the section
* Do one: You do what I just did
* Teach one: You take what you did and .red[teach] someone else

## 3.17 Lets Get .red[Back] to it!

# 4 Explore sqlite3 CLI.

## 4.1 Load the throw_away_csv

### 4.1.1 Load the throw_away_csv

```shell
pwd  # should be `Data_Manipulation_with_Modern_SQL`
mkdir db # A subdir for the DB helps us keep things clean
sqlite3 db/sqlite.db # This is a personal convention
```

### 4.1.2 Load the throw_away_csv

```
$ sqlite3
SQLite version 3.20.0 2017-08-01 13:24:15
Enter ".help" for usage hints.
Connected to a transient in-memory database.
Use ".open FILENAME" to reopen on a persistent database.
```

### 4.1.3 Now in the SQLite cli

```sql
.mode csv
.import data/throw_away_csv.txt throw_away
```

### 4.1.4 look around at what got loaded

```sql
.help
```

### 4.1.5 look around at what got loaded

```sql
.schema
```

### 4.1.6 look around at what got loaded

```sql
.mode line
select * from throw_away;
```

### 4.1.7 look around at what got loaded

```sql
.mode line
select * from throw_away limit 1;
```

### 4.1.8 look around at what got loaded

```sql
.mode line
select * from throw_away limit 2 offset 4;
```

### 4.1.9 look around at what got loaded

```sql
.mode column 
.header on
select * from throw_away limit 10;
```

### 4.1.10 Save it as a CSV

```sql
.output example.csv
.mode csv
.header on
select * from throw_away;
```

### 4.1.11 Clean up the mess

```sql
drop table throw_away;
.schema
```

### 4.1.12 Clean up the mess

```sql
.quit
```

### 4.1.13 Look at the csv

At the .red[shell] prompt:

```shell
less example.csv
```

## 4.2 Work through the Section

# 5 Load The CFS Data

## 5.1 Load The .red[CFS] Data

https://www.census.gov/econ/cfs/

## 5.2 Load The CFS Data

The sources are detailed in Sylabus.md

## 5.3 Load the CFS data: .red[Challenge.] 

The `data/` directory has all the source data already extracted.
Still it is not too hard to pull the original source data and extract
them yourself.  I used LibreOffice .red[Calc].

### 5.3.1 Load the CFS data: .red[Challenge]. 

The `data/` directory has all the source data. Still it
is not too hard to pull the original source data and extract them 
yourself.  I used LibreOffice Calc.

Go to the source websites, Get the data and `save as` each tab as CSV
files.  The sources are all in the Sylabus.md file.

.red[This is .bold[not] a required step.]

### 5.3.2 Load the CSV files into the database.

Start by making .red[sure] that the database is empty. 

### 5.3.3 Load the CSV files into the database.

Start by making sure that the database is empty. 
The bone head easy way is to simply remove the database file:

### 5.3.4 Load the CSV files into the database.

Start by making sure that the database is empty. 
The bone head easy way is to simply .red[remove] the database file:

```shell
rm db/sqlite.db
```

### 5.3.5 Load the CSV files into the database.

Start by making sure that the database is empty. 
The bone head easy way is to simply remove the database file:

```shell
rm db/sqlite.db
```

This is usually a bad idea on big distributed shared
database server. 

### 5.3.6 Start the SQLite CLI

```shell
sqlite3 db/sqlite.db
```

### 5.3.7 Now import the bulk data into the database

```sql
.mode csv
.import data/cfs_2012_pumf_csv.txt cfs
``` 

This might take a few seconds.

### 5.3.8 Prove the data is in the database

```sql
.schema
select * from cfs limit 10;
select count(1) from cfs;
```

### 5.3.9 Add some more lookup data 

```sql
.mode csv
.import data/national_county.txt national_county
select count(1) from national_county;
```

### 5.3.10 Exit the sqlite3 cli

```sql
.help
.quit
```

### 5.3.11 Add the rest of the metadata from the user guide

Look at the `load_meta` script and the `generate_import` helper

```shell
cat load_meta
```

### 5.3.12 Add the rest of the metadata from the user guide

Look at the `load_meta` script and the `generate_import` helper

```shell
cat generate_import
```

### 5.3.13 Load the rest of the metadata

```shell
./load_meta
```

### 5.3.14 Check what we have loaded

```shell
sqlite3 db/sqlite.db
```

### 5.3.15 Check what we have loaded

```sql
.schema
```

### 5.3.16 Check what we have loaded

```sql
.schema
select count(1) from STATE_FIPS;
```

### 5.3.17 Look at data from the other tables we loaded.

```sql
.mode line
select * from NAICS_North_American_Industry_Classification_System_Codes
limit 1
;
```

### 5.3.18 Look at data from the other tables we loaded.

An overly smartass way way to do it.

```shell
echo "select name from sqlite_master where type = 'table'" |
sqlite3 db/sqlite.db |
while read t
do 
    echo $t
    echo "select * from $t limit 1;" |
    sqlite3 -line db/sqlite.db
    echo
    read -p continue < /dev/tty
done
```

## 5.4 Work through the section

# 6 Simple Queries

## 6.1 Simple Queries

Gain some confidence that the data has been loaded and play with
it a bit.

## 6.2 Simple Queries

Gain some confidence that the data has been loaded and play with
it a bit. 

Work with one table at a time.

### 6.2.1 How many shipments from Colorado?

### 6.2.2 How many shipments from Colorado?

```sql
.schema
.mode line
select * from state_fips where state_name like 'Colorado' limit 1;
select * from cfs where orig_state = '08' limit 1;
select count(1) from cfs where orig_state = '08';
```

### 6.2.3 How many counties are in New York state?

### 6.2.4 How many counties are in New York state?

```sql
select * from national_county where state = 'NY';
select count(1) from national_county where state = 'NY';
```

### 6.2.5 What state has the most counties?

### 6.2.6 What state has the most counties?

```sql
select state, count(1) as count 
from national_county group by statefp
order by count desc limit 1
```

### 6.2.7 How much money did Alabama make?

### 6.2.8 How much money did Alabama make?

```sql
.mode column
.header on
select state from state_fips where state_name = 'Alabama';
select sum(SHIPMT_VALUE) as total_value from  cfs
where orig_state = '01';
```

### 6.2.9 Think up a query of your own

### 6.2.10 Think up a query of your own

```sql
select count(1) as count, mode from cfs
group by mode
order by count
;
```

## 6.3 Work Through The Section.

# 7 Break

# 8 What is an RDBMS?

## 8.1 What is an RDBMS?

Relational Database Management Systems (RDBMS) view the world through
rectangular glasses.

## 8.2 What is an RDBMS?

Relational Database Management Systems (RDBMS) view the world through
rectangular glasses.

* Tables of rows made out of named fields.

## 8.3 What is an RDBMS?

Relational Database Management Systems (RDBMS) view the world through
rectangular glasses.

* Tables of rows made out of named fields.
* Fixed number of columns. 

## 8.4 What is an RDBMS?

Relational Database Management Systems (RDBMS) view the world through
rectangular glasses.

* Tables of rows made out of named fields.
* Fixed number of columns. 
* Usually tables have a "primary key". 

## 8.5 What is an RDBMS?

Relational Database Management Systems (RDBMS) view the world through
rectangular glasses.

* Tables of rows made out of named fields.
* Fixed number of columns. 
* Usually tables have a "primary key". 
* The primary key can be a single column or .red[many] columns. 

## 8.6 What is an RDBMS?

Relational Database Management Systems (RDBMS) view the world through
rectangular glasses.

* Tables of rows made out of named fields.
* Fixed number of columns. 
* Usually tables have a "primary key". 
* The primary key can be a single column or many columns. 
* Often the primary key is .red[synthetic].

## 8.7 What is an RDBMS.

Relational Database Management Systems (RDBMS) view the world through
rectangular glasses.

* Tables of rows made out of named fields.
* Fixed number of columns. 
* Usually tables have a "primary key". 
* The primary key can be a single column or many columns. 
* Often the primary key is synthetic.
* Tables also often contain .red[foreign] keys.

## 8.8 What is an RDBMS.

Relational Database Management Systems (RDBMS) view the world through
rectangular glasses.

* Tables of rows made out of named fields.
* Fixed number of columns. 
* Usually tables have a "primary key". 
* The primary key can be a single column or many columns. 
* Often the primary key is synthetic.
* Tables also often contain foreign keys.
* The "Relational" in RDBMS is the .red[association] of the foreign
  key in one table with a primary key in another table.  

## 8.9 What is an RDBMS.

Relational Database Management Systems (RDBMS) view the world through
rectangular glasses.

* Tables of rows made out of named fields.
* Fixed number of columns. 
* Usually tables have a "primary key". 
* The primary key can be a single column or many columns. 
* Often the primary key is synthetic.
* Tables also often contain foreign keys.
* The "Relational" in RDBMS is the association of the foreign
  key in one table with a primary key in another table.  
* Entity Relationship Diagrams (ERD) (demo)

## 8.10 No exercises in this section.  Any questions?

# 9 Structure

## 9.1 Table Structure

## 9.2 Normalization

* Relational Calculus -- A kind of math
* https://en.wikipedia.org/wiki/Database_normalization

## 9.3 Normalization

* Relational Calculus -- A kind of math
* https://en.wikipedia.org/wiki/Database_normalization
* 1NF, 2NF, 3NF and so on.

## 9.4 Normalization

* Relational Calculus -- A kind of math
* https://en.wikipedia.org/wiki/Database_normalization
* 1NF, 2NF, 3NF and so on.
* Pretty important for enterprise class databases.

## 9.5 Normalization

* Reduce duplication of data stored

## 9.6 Normalization

* Reduce duplication of data stored
* Reduce complication of making changes

## 9.7 Normalization

* Reduce duplication of data stored
* Reduce complication of making changes
* Gets pretty academic pretty fast.

## 9.8 Normalization

* Reduce duplication of data stored
* Reduce complication of making changes
* Gets pretty academic pretty fast.
* Most important in OLTP.

## 9.9 Normalization

* Not going to go into it any further.

## 9.10 Entity Attribute Value Model

Another common structure

## 9.11 Entity Attribute Value Model

https://en.wikipedia.org/wiki/Entity%E2%80%93attribute%E2%80%93value_model

## 9.12 Entity Attribute Value Model

```
create table eav (
    id uuid primary key,
    key text,
    type uuid,
    value text
);
```

### 9.12.1 Entity Attribute Value Model

* This is usually an anti-pattern.

```
create table eav (
    id uuid primary key,
    key text,
    type uuid,
    value text
);
```

## 9.13 Entity Attribute Value Model

* This is usually an anti-pattern. 
  Inner Platform

```
create table eav (
    id uuid primary key,
    key text,
    type uuid,
    value text
);
```

## 9.14 Entity Attribute Value Model

* This is usually an anti-pattern.
* Sometimes useful when your app persists user preferences

```
create table eav (
    id uuid primary key,
    userid uuid,
    key text,
    value text
);
```

## 9.15 Encoded Fields

## 9.16 Encoded Fields

* Partially structured data

## 9.17 Encoded Fields

* Partially structured data

```
create table email (
    id uuid primary key,
    sentfrom text, 
    sentto text,
    recieved timestamp with timezone,
    subject text,
    headers json,
    body text
);
```

## 9.18 Other normalizations

* Data Warehouse 

## 9.19 Other normalizations

* Data warehouses
* Time Series Data

## 9.20 Other normalizations

* Data warehouse
* Time Series data
* Tracking Metadata

## 9.21 Other normalizations

* Data warehouse
* Time Series data
* Tracking Metadata (standard columns)

## 9.22 The Naive Approach

## 9.23 The Naive Approach

* Do as little as needed to ingest the data as possible.

## 9.24 The Naive Approach

* Do as little as needed to get the data loaded
* Programmers are horrible at designing:

## 9.25 The Naive Approach

* Do as little as needed to get the data loaded
* Programmers are horrible at designing:
  * logging output

## 9.26 The Naive Approach

* Do as little as needed to get the data loaded
* Programmers are horrible at designing:
  * logging output
  * report output

## 9.27 The Naive Approach

* Do as little as needed to get the data loaded
* use sed,awk,grep

## 9.28 The Naive Approach

* Do as little as needed to get the data loaded
* use Perl

## 9.29 The Naive Approach

* Do as little as needed to get the data loaded
* use Python

## 9.30 The Naive Approach

* Do as little as needed to get the data loaded
* use creativity, guile and .red[intestinal] fortitude.

## 9.31 The Naive Approach

* Do as little as needed to get the data loaded
* Use creativity, guile and intestinal fortitude.
* Convert source text into csv with a header then import into sqlite.

## 9.32 The Naive Approach

* Do as little as needed to get the data loaded
* Use creativity, guile and intestinal fortitude.
* Convert source text into csv with a header then import into sqlite.
* Use SQL to reformat data into a usable down stream format.

## 9.33 The Naive Approach

* The throw away table is an example of this.

## 9.34 The Naive Approach

* The throw away table is an example of this.

```
cat bin/mk_throw_away
```

## 9.35 No exercises in this section.  Any questions?

# 10 SQL, DDL, DML, DCL

## 10.1 SQL, DDL, DML, DCL

* SQL is the Structured Query Language.

## 10.2 SQL, DDL, DML, DCL

* SQL is the Structured Query Language.
* DDL, DML, DCL are Divisions of SQL into sub-languages.

## 10.3 DDL

DDL is .red[Data Definition Language]. 
Primarily `CREATE`, `ALTER`, `DROP`, `TRUNCATE`, `DELETE`.  

## 10.4 DDL

DDL is Data Definition Language.  Primarily `CREATE`, `ALTER`, `DROP`,
`TRUNCATE`, `DELETE`.  

We are mostly letting .green[sqlite3] do this for us.

## 10.5 DML

DML is the .red[Data Manipulation Language].

* `SELECT` ... `FROM` ... `WHERE` ...
* `INSERT INTO` ... `VALUES` ...
* `UPDATE` ... `SET` ... `WHERE` ...
* `DELETE FROM` ... `WHERE` ...

We will work mostly with `SELECT` in this class.

## 10.6 DCL

DCL is .red[Data Control Language]. Everything else.
User management, permissions, and so on.
Most database systems get very extensive in this section.
Facilitating enabling and disabling different kinds of access to
various local and remote resources.

## 10.7 DCL

DCL is the .red[Data Control Language]. Everything else. User management,
permissions, and so on.
Most database systems get very extensive in this section.
Facilitating enabling and disabling different kinds of access to
various local and remote resources.

Sqlite3 sidesteps a lot of this by being single user and single
file database.

## 10.8 No exercises in this section.  Any questions?

# 11 SQLite

## 11.1 SQLite

* https://sqlite.org
* https://sqlite.org/lang.html
* https://www.youtube.com/watch?v=Jib2AmRb_rk

## 11.2 SQLite Blurb 

"SQLite is an in-process library that implements a
self-contained, serverless, zero-configuration, transactional
SQL database engine. The code for SQLite is in the public domain
and is thus free for use for any purpose, commercial or private.
SQLite is the most widely deployed database in the world with
more applications than we can count, including several
high-profile projects."

https://sqlite.org/about.html

## 11.3 SQLite Thoughts

## 11.4 SQLite Thoughts

Most other database systems are

* Strongly Typed

## 11.5 SQLite Thoughts

Sqlite is 

* 'Stringly' Typed 

## 11.6 SQLite Thoughts

Sqlite is 

* 'Stringly' Typed (.red[my] words)

## 11.7 SQLite Thoughts

Most other database systems are 

* Strongly Typed
* Highly Distributed

## 11.8 SQLite Thoughts

Sqlite is 

* 'Stringly' Typed
* Minimally Distributed

## 11.9 SQLite Thoughts

Most other database systems are 

* Strongly Typed
* Highly Distributed
* Large and .red[Complex]

## 11.10 SQLite Thoughts

Sqlite is 

* 'Stringly' Typed
* Minimally Distributed
* Small but Highly .red[Capable]

## 11.11 SQLite Thoughts

Sqlite is 

* 'Stringly' Typed
* Minimally Distributed
* Small and Highly Capable

This makes it a good DB for this kind of class.

## 11.12 SQLite Docs

* [SQL As Understood By SQLite](https://sqlite.org/lang.html)
* [Aggregate Functions](https://sqlite.org/lang_aggfunc.html)
* [Core Functions](https://sqlite.org/lang_corefunc.html)

## 11.13 No exercises in this section.  Any questions?

# 12 A little DDL

## 12.1 Rename some tables 

### 12.1.1 Rename some tables NAICS

```sql
alter table NAICS_North_American_Industry_Classification_System_Codes
    rename to NAICS;
```

### 12.1.2 Rename some tables SCTG

```sql
alter table SCTG_Standard_Classification_of_Transported_Goods_Codes
    rename to SCTG;
```

## 12.2 A few useful indexes

## 12.3 A few useful indexes

Indexes are more a practical aspect of RDBMS than an organizational
one. They provide clues about how specific data can be accessed
without having to traverse the whole data structure. 

### 12.3.1 A few useful indexes

```sql
.echo on
create index cfs_id on cfs(SHIPMT_ID);
create index cfs_orig_state on cfs(ORIG_STATE);
create index cfs_orig_ma on cfs(ORIG_MA);
create index cfs_dest_state on cfs(DEST_STATE);
create index cfs_dest_ma on cfs(DEST_MA);
create index cfs_naics on cfs(NAICS);
create index cfs_sctg on cfs(SCTG);

create index cfs_areas_state on cfs_areas(state);
create index cfs_areas_ma on cfs_areas(ma);
```

## 12.4 A bit more DDL

### 12.4.1 Restructure the `CFS_Areas` table

* Has some confusing column names from the source data

### 12.4.2 Restructure the `CFS_Areas` table

* Has some confusing column names from the source data
* Has a row that might have been a header if my ingestion code was
  smarter

### 12.4.3 Restructure the `CFS_Areas` table

* Has some confusing column names from the source data
* Has a row that might have been a header if my ingestion code was
  smarter

```sql
.mode column
.header on
select * from CFS_Areas limit 3;
```

### 12.4.4 Restructure the `CFS_Areas` table

```sql
.echo on

alter table CFS_Areas
    rename to CFS_Areas_Alt
;

create table CFS_Areas as
    select ORIG_MA as MA,
           ORIG_STATE as STATE,
           ORIG_CFS_AREA as CFS_AREA,
           MA as MA_Type,
           Description
    from CFS_Areas_Alt
;

delete from CFS_Areas where STATE = 'DEST_STATE';

drop table CFS_Areas_Alt;
```

### 12.4.5 Restructure the `CFS_Areas` table

```sql
.mode column
.header on
select * from CFS_Areas limit 3;
```

## 12.5 Work Through The Section.

# 13 More Complex Queries

## 13.1 More Complex Queries

Single table queries are cool and all but...

## 13.2 Subqueries

* How many shipments from Colorado
* What state originated the most shipments? 
* Show me shipments by industry classification

### 13.2.1 How many shipments from Colorado

```sql
.mode columns
.header on

select count(1) as shipments
    from cfs
    where orig_state = (
        select state from state_fips
            where state_name like 'Colorado'
    )
;
```

### 13.2.2 What state originated the most shipments?

```sql
.mode columns
.headers on

select max(shipments), orig_state, (select STATE_NAME from STATE_FIPS where state = orig_state)
    from (
        select orig_state, count(1) as shipments
            from cfs
            group by orig_state
    )
;
```

### 13.2.3 Show me shipments by industry classification

```sql
.mode columns
.header on

select count(1) as Shipments, 
    (select Description from NAICS where cfs.naics = naics.naics)
    as Description,
    NAICS
    from cfs
    group by NAICS
    order by NAICS
;
```

## 13.3 Union

```
select blablabla
union 
select blablabla
```

Results have to have the same "shape".

### 13.3.1 Union

```sql
.mode columns
.header on

select count(1) as Shipments,
 (select Description from sctg where "SCTG Group" = '01-05') as Description
from cfs where SCTG >= 01 and SCTG <= 05
union
select count(1) as Shipments,
 (select Description from sctg where "SCTG Group" = '15-19') as Description
from cfs where SCTG >= 15 and SCTG <= 19
;
```

## 13.4 Transactions

Treating a sequence of sql statements as a single "atomic" action.

## 13.5 Transactions

Treating a sequence of sql statements as a single "atomic" action.
Note: sqlite transactions appear to be DML only.

## 13.6 Transactions

```
begin;

bla
bla
bla

commit; -- or rollback
```

### 13.6.1 Transactions

```sql
.echo on
begin;

insert into Mode_of_Transportation_Codes (mode, description) values
(30, 'paper airplane');

select * from Mode_of_Transportation_Codes;

rollback;

select * from Mode_of_Transportation_Codes;
```

### 13.6.2 Transactions

Note: Transactions impact performance

### 13.6.3 Transactions

Note: Transactions impact performance

* Too many .red[small] ones

### 13.6.4 Transactions

Note: Transactions impact performance

* Too many small ones
* Too many big ones

### 13.6.5 Transactions

Note: Transactions impact performance

* Too many small ones
* Too many big ones
* Holding one for .red[too] long

### 13.6.6 Transactions

Note: If no transaction is in effect then each statement is a
transaction.

## 13.7 Views and Temporary tables

## 13.8 Views

A form of stored query

### 13.8.1 Views SCTG By Group

```sql
create view "SCTG By Group" as 
select SCTG, '01-05' as Grp from SCTG where SCTG >= '01' and SCTG <= '05'
union
select SCTG, '06-09' as Grp from SCTG where SCTG >= '06' and SCTG <= '09'
union
select SCTG, '10-14' as Grp from SCTG where SCTG >= '10' and SCTG <= '14'
union
select SCTG, '15-19' as Grp from SCTG where SCTG >= '15' and SCTG <= '19'
union
select SCTG, '20-24' as Grp from SCTG where SCTG >= '20' and SCTG <= '24'
union
select SCTG, '25-30' as Grp from SCTG where SCTG >= '25' and SCTG <= '30'
union
select SCTG, '31-34' as Grp from SCTG where SCTG >= '31' and SCTG <= '34'
union
select SCTG, '35-38' as Grp from SCTG where SCTG >= '35' and SCTG <= '38'
union
select SCTG, '39-99' as Grp from SCTG where SCTG >= '39' and SCTG <= '99'
;

.mode column
.header on
select * from "SCTG By Group" order by Grp;
```

## 13.9 Temporary Tables

A temporary table lasts as long as the current session is active.

### 13.9.1 Temporary Tables 

```sql
.echo on

create temporary table export_shipments as
select * from cfs where EXPORT_YN = 'Y'
;

.mode column
.header on
select count(1) from export_shipments;
```

## 13.10 Work through the Section

# 14 Joins

## 14.1 Joins

* Joins inner, left, right, outer, cross
* https://en.wikipedia.org/wiki/Join_(SQL)
* http://www.codeproject.com/KB/database/Visual_SQL_Joins/Visual_SQL_JOINS_orig.jpg

## 14.2 Examples

* How many Shipments from Colorado?
* What County Originated largest total value of Basic Chemicals Shipments?
* What are the total values for all shipments by mode?
* How many shipments were exported?
* Break down total shipment weight by SCTG group

### 14.2.1 How many Shipments from Colorado?

```sql
.mode column
.header on

select count(1) as Shipments, state_name from cfs
join state_fips s on state = orig_state
where s.stusab = 'CO'
;
```

### 14.2.2 What County Originated largest total value of Basic Chemicals Shipments?

```sql
.echo on

create temporary table sctg_grp as 
    select * from "SCTG By Group" 
    join SCTG using(sctg)
;

create temporary table bc_sctg as 
    select sctg from sctg_grp
    where grp in (
        select grp from sctg_grp where description = 'Basic Chemicals'
    )
;

create temporary table tsv as 
    select sum(SHIPMT_VALUE) Total_Shipment_Value,
        ORIG_STATE, ORIG_MA, ca.*
        from cfs
        join CFS_Areas ca on orig_state = ca.state and ORIG_MA = ca.ma 
        where cfs.sctg in (select sctg from bc_sctg )
        group by ORIG_STATE, ORIG_MA
        order by Total_Shipment_Value
;

.mode column
.header on
select max(Total_Shipment_Value), Description from tsv
;

```

### 14.2.3 What are the total values for all shipments by mode?

```sql
.header on
.mode column

select sum(SHIPMT_VALUE) Total_Value, Description from cfs
join Mode_of_Transportation_Codes mt using(mode)
group by mode
union 
select sum(SHIPMT_VALUE) Total_Value, 'Grand Total' from cfs
order by Total_Value
;
```

### 14.2.4 How many shipments were exported?

```sql
.mode column
.header on
select count(1) Shipments,
    case EXPORT_CNTRY
    when 'O' then 'Other'
    when 'M' then 'Mexico'
    when 'C' then 'Canada'
    end as Country
from cfs
group by EXPORT_CNTRY having EXPORT_YN = 'Y'
union
select count(1) Shipments, 'Total'
from cfs
where EXPORT_YN = 'Y'
;

```

### 14.2.5 How many shipments were exported?

That case is pretty ugly.  Lets make it a lookup.

```sql
drop table if exists country_code;
create table country_code (
    country text,
    code text
)
;

insert into country_code (code, country)
values 
( 'O', 'Other' ),
( 'M', 'Mexico'),
( 'C', 'Canada')
;

.mode column
.header on
select * from country_code;

```

### 14.2.6 How many shipments were exported?

Now .red[with] join!


```sql
.mode column
.header on
select count(1) Shipments, Country
from cfs
join country_code on EXPORT_CNTRY = code
group by EXPORT_CNTRY having EXPORT_YN = 'Y'
union
select count(1) Shipments, 'Total'
from cfs
where EXPORT_YN = 'Y'
;

```

### 14.2.7 Break down total shipment weight by SCTG group

```sql
.header on
.mode column
.width 15 10 20 
select sum("SHIPMT_WGHT") TotalWeight, GRP,
    s.Description
from cfs
join "SCTG By Group" using(SCTG)
join (select * from SCTG where "SCTG Group" is not null) s 
    on grp = "SCTG Group"
group by grp
union
select sum("SHIPMT_WGHT") TotalWeight, "Tot", "Total Weight"
from cfs
order by TotalWeight
;

```

## 14.3 Work the Section

# 15 Break

# 16 CTE

*  What County Originated largest total value of Basic Chemicals Shipments?

### 16.3.1 What County Originated largest total value of Basic Chemicals Shipments?

```sql
.echo on

.mode column
.header on

with sctg_grp as ( select * from "SCTG By Group" 
        join SCTG using(sctg)
    ),
    bc_sctg as (
        select sctg from sctg_grp
        where grp in (
            select grp from sctg_grp where description = 'Basic Chemicals'
        )
    ),
    tsv as (
    select sum(SHIPMT_VALUE) Total_Shipment_Value,
        ORIG_STATE, ORIG_MA, ca.*
        from cfs
        join CFS_Areas ca on orig_state = ca.state and ORIG_MA = ca.ma 
        where cfs.sctg in (select sctg from bc_sctg )
        group by ORIG_STATE, ORIG_MA
        order by Total_Shipment_Value
    )
    select max(Total_Shipment_Value), Description from tsv;
```

### 16.3.2 Break down total shipment weight by SCTG group

```sql
.header on
.mode column
.width 15 10 20 

with stg as (
    select * from SCTG where "SCTG Group" is not null
    )
select sum("SHIPMT_WGHT") TotalWeight, GRP,
    stg.Description
    from cfs
    join "SCTG By Group" using(SCTG)
    join stg on grp = "SCTG Group"
    group by grp
union
select sum("SHIPMT_WGHT") TotalWeight, "Tot", "Total Weight"
    from cfs
order by TotalWeight
;

```

## 16.4 Work through the Section

# 17 Recursive CTE

https://sqlite.org/lang_with.html

### 17.4.1 Generate a Sequence With a Recursive CTE

```sql
WITH RECURSIVE
  count(x) AS (VALUES(1) UNION SELECT x+1 FROM count WHERE x<10)
SELECT x FROM count
;
```

### 17.4.2 Factorial

* f(n) = f(n-1) * N

```sql
WITH RECURSIVE
    fact(x,y) as ( values(1, 1)
        union all
        select x+1, y*(x+1) from fact  where x < 10 
    )
SELECT * FROM fact
;
```


### 17.4.3 Fibonacci

* f(n) = f(n-1) + f(n-2)

```sql
with recursive
    fib(i, f1, f2) as ( values(1, 1, 1)
        union all
        select i+1, f1+f2, f1
        from fib
        where i < 10
    )
select * from fib
;
```

### 17.4.4 Mandelbrot set

* https://en.wikipedia.org/wiki/Mandelbrot_set
* z^2 +c does not diverge (z & c are complex)

```sql
WITH RECURSIVE
  xaxis(x) AS (VALUES(-2.0) UNION ALL SELECT x+0.05 FROM xaxis WHERE x<1.2),
  yaxis(y) AS (VALUES(-1.0) UNION ALL SELECT y+0.1 FROM yaxis WHERE y<1.0),
  m(iter, cx, cy, x, y) AS (
    SELECT 0, x, y, 0.0, 0.0 FROM xaxis, yaxis
    UNION ALL
    SELECT iter+1, cx, cy, x*x-y*y + cx, 2.0*x*y + cy FROM m 
     WHERE (x*x + y*y) < 4.0 AND iter<28
  ),
  m2(iter, cx, cy) AS (
    SELECT max(iter), cx, cy FROM m GROUP BY cx, cy
  ),
  a(t) AS (
    SELECT group_concat( substr(' .+*#', 1+min(iter/7,4), 1), '') 
    FROM m2 GROUP BY cy
  )
SELECT group_concat(rtrim(t),x'0a') FROM a;
```

## 17.5 Slightly More Practical

### 17.5.1 Hierarchical Search

```

            .------Alice---------.
            |                    |
            v                    v
      .----Bob----.       .----Cindy----.
      |           |       |             |
      v           v       v             v
    Dave        Emma     Fred         Gail
```

### 17.5.2 Hierarchical Search: Data

```sql
CREATE TABLE org(
  name TEXT PRIMARY KEY,
  boss TEXT REFERENCES org
) WITHOUT ROWID;
INSERT INTO org VALUES('Alice',NULL);
INSERT INTO org VALUES('Bob','Alice');
INSERT INTO org VALUES('Cindy','Alice');
INSERT INTO org VALUES('Dave','Bob');
INSERT INTO org VALUES('Emma','Bob');
INSERT INTO org VALUES('Fred','Cindy');
INSERT INTO org VALUES('Gail','Cindy');
```

### 17.5.3 Hierarchical Search: Traversal Breadth First

```sql
WITH RECURSIVE
  under_alice(name,level) AS (
    VALUES('Alice',0)
    UNION ALL
    SELECT org.name, under_alice.level+1
      FROM org JOIN under_alice ON org.boss=under_alice.name
     ORDER BY 2
  )
SELECT substr('..........',1,level*3) || name FROM under_alice;

```

### 17.5.4 Hierarchical Search: Traversal Depth First

```sql
WITH RECURSIVE
  under_alice(name,level) AS (
    VALUES('Alice',0)
    UNION ALL
    SELECT org.name, under_alice.level+1
      FROM org JOIN under_alice ON org.boss=under_alice.name
     ORDER BY 2 DESC
  )
SELECT substr('..........',1,level*3) || name FROM under_alice;
```

## 17.6 Work through the Section

# 18 Conclusion

## 18.1 Next Steps

* Learn a programming language
* Learn more about SQL and RDBMS while learing more about NoSQL too.
  * [SQL for Smarties](https://www.elsevier.com/books/joe-celkos-sql-for-smarties/celko/978-0-12-800761-7)

## 18.2 Questions



# 19 Colophon

this would not have been possible without all those who made Linux and
the distros I have used and tested on.  I would like to point out a few
projects that were critical to this project.

* https://remarkjs.com/
* https://daringfireball.net/projects/markdown/
* https://github.com/partageit/markdown-to-slides
* https://about.gitlab.com/
* https://git-scm.com/
* https://sqlite.org/index.html
* https://www.perl.org/
* https://www.gnu.org/software/bash/
