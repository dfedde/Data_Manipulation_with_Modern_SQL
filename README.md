# Course Concept

My idea is on the theme "Data Manipulation using Modern SQL".
The RDBMS for the class will be SQLite3 since it is quite easy
to install and use in a single user context.   Topics would
include a quick review of "relational calculus".  That's mostly
a big word that says "We view the world in named grids with
named columns".  Touch on the concept of primary keys, both
natural and synthetic. Normalization as as an ideal but not a
goal. And get into the meat:  Loading data. Single table
queries, David's favorite: JOIN. Indexes, Data conditioning
including ALTER TABLE and UPDATE, Probably also CREATE TABLE AS
SELECT.
 
I'd also like to get as far as Common Table Expressions (CTE)
and recursive CTE.  That is where all the cool stuff is with
SQL these days.  Unfortunately sqlite3 does not yet support
window functions and so we need to leave something for the next
step.

# Approach

My normal approach to classes like this is: See One, Do One,
Teach One.
In See One I'll go through the whole demo at conversation speed
including hand waving explanations of the theoretical parts.
in Do One I'll bust it out into labs and go deeper in to the
asides on theory and design stuff. In Teach One I try to ensure
that the class knows enough to teach pay it forward. We review
what we did and delve as deep as we have time for and remaining
interest.

I'll use one of the freely available  data sets and try to pick
one that is topical because it is always more fun for me that
way.   I'll avoid using any complex tools other than the RDBMS
and all will be stuff that comes with a modern linux. My
courseware is always released to the public domain in so far as
I am free to do so.  And all the free parts will be on gitlab
somewhere.

# Data set
https://www.census.gov/econ/cfs/2012/cfs_2012_pumf_csv.zip
