# Public Domain

To the extend that it is possible and legal to do so I release this
content to the public domain.  I relinquish all rights to this content
and grant for you to use in any way you see fit.

This grant does not extend to any of the tools I used to generate or
develop this content.  Each of those works has its own copyright and
license statement.

Chris Fedde 2017

https://en.wikipedia.org/wiki/Public_domain
