-- 16.3.2 Break down total shipment weight by SCTG group

.header on
.mode column
.width 15 10 20 

with stg as (
    select * from SCTG where "SCTG Group" is not null
    )
select sum("SHIPMT_WGHT") TotalWeight, GRP,
    stg.Description
    from cfs
    join "SCTG By Group" using(SCTG)
    join stg on grp = "SCTG Group"
    group by grp
union
select sum("SHIPMT_WGHT") TotalWeight, "Tot", "Total Weight"
    from cfs
order by TotalWeight
;

