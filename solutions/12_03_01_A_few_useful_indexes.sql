-- 12.3.1 A few useful indexes

.echo on
create index cfs_id on cfs(SHIPMT_ID);
create index cfs_orig_state on cfs(ORIG_STATE);
create index cfs_orig_ma on cfs(ORIG_MA);
create index cfs_dest_state on cfs(DEST_STATE);
create index cfs_dest_ma on cfs(DEST_MA);
create index cfs_naics on cfs(NAICS);
create index cfs_sctg on cfs(SCTG);

create index cfs_areas_state on cfs_areas(state);
create index cfs_areas_ma on cfs_areas(ma);
