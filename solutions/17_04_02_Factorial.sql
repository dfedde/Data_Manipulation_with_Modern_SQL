-- 17.4.2 Factorial

WITH RECURSIVE
    fact(x,y) as ( values(1, 1)
        union all
        select x+1, y*(x+1) from fact  where x < 10 
    )
SELECT * FROM fact
;
