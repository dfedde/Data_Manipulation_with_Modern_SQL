-- 14.2.4 How many shipments were exported?

.mode column
.header on
select count(1) Shipments,
    case EXPORT_CNTRY
    when 'O' then 'Other'
    when 'M' then 'Mexico'
    when 'C' then 'Canada'
    end as Country
from cfs
group by EXPORT_CNTRY having EXPORT_YN = 'Y'
union
select count(1) Shipments, 'Total'
from cfs
where EXPORT_YN = 'Y'
;

