-- 17.4.1 Generate a Sequence With a Recursive CTE

WITH RECURSIVE
  count(x) AS (VALUES(1) UNION SELECT x+1 FROM count WHERE x<10)
SELECT x FROM count
;
