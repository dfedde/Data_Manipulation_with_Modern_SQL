-- 13.9.1 Temporary Tables 

.echo on

create temporary table export_shipments as
select * from cfs where EXPORT_YN = 'Y'
;

.mode column
.header on
select count(1) from export_shipments;
