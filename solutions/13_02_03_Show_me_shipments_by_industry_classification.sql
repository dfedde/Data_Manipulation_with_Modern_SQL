-- 13.2.3 Show me shipments by industry classification

.mode columns
.header on

select count(1) as Shipments, 
    (select Description from NAICS where cfs.naics = naics.naics)
    as Description,
    NAICS
    from cfs
    group by NAICS
    order by NAICS
;
